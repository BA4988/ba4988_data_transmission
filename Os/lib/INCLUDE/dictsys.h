/******************************************************************************************
*				
*	Copyright (c)2002 , 广东步步高教育电子分公司
*	All rights reserved.
**
*	文件名称：dictsys.h
*	文件标识：BA818系统
*	摘    要：BA818系统数据类型和函数接口定义
**
*	修改历史：
*	版本    	日期    	 作者    	 改动内容和原因
*   ------		-------		---------	------------------------------
*	1.0    		2002.7.26   任新村      完成基本内容
*******************************************************************************************/

#ifndef		DICTSYS_H
#define		DICTSYS_H

#include	"keytable.h"

#define		COMPILER_2500AD
/*#define		DICT_DEBUG*/

/******************************************************************************************
*						系统数据类型定义
*******************************************************************************************/

typedef		unsigned	char		U8;
typedef		unsigned	int			U16;
typedef		unsigned	long		U32;

#define		TRUE		1
#define		FALSE		0	

#ifdef		DICT_DEBUG
#define		FAR	
#else
#define		FAR			banked
#endif

/******************************************************************************************
*						系统硬件信息定义
*******************************************************************************************/
#define		SCR_WIDTH	159				/* 屏幕宽 */
#define		SCR_HEIGHT	96				/* 屏幕高 */

#define		MEM_HEAP_START		0x2000	/* 堆首地址 */		
#define		MEM_HEAP_SIZE		0x2000	/* 堆大小 */

/******************************************************************************************
*						消息定义
*******************************************************************************************/

typedef		struct	tagMsgType
{
	U8		type;
	U16		param;
}MsgType,*PtrMsg;

#define		WM_KEY			0x01		/* 按键值，只有行列信息 */

#define		WM_CHAR_ASC		0x02		/* 输入 ASCII码 */
#define		WM_CHAR_HZ		0x03		/* 输入汉字 */
#define		WM_CHAR_MATH	0x04			/* 数学功能键 */
#define		WM_CHAR_FUN		0x05		/* 输入功能键 */

#define		WM_TIMER		0x06		/* 定时到 */
#define		WM_UART			0x07		/* 串口接收到数据 */

#define		WM_POWER		0x08		/* 电源 */

#define		WM_ALERT		0x09		/* 闹钟到 ，行程到 */
#define		WM_COMMAND		0x0a		/* 界面相关 */	
/*-----------------------------------------------------------------------------------------
*			WM_COMMAND  类消息 对应消息值定义				
*-----------------------------------------------------------------------------------------*/
#define		CMD_CHN_INPUT_OPEN			1
#define		CMD_CHN_INPUT_CLOSE			2


/******************************************************************************************/


#define		MAX_MSG_NUM		8			/*	消息队列长度 */
#define		MAX_KEY_NUM		64			/*  按键个数 */
/******************************************************************************************
*						Gui结构类型定义
*******************************************************************************************/
#define			MENU_STYLE_ICON					0x01
#define			MENU_STYLE_ICON_NMASK			0xfe

typedef		struct	tagGuiMenuItemType
{
	U8		strLen;				/*  该项菜单字符串长度 */
	U8		*str;				/*  菜单项字符串 */		
	U8		*bmpData0;			/*  普通菜单项的图标 */
	U8		*bmpData1;			/*  选中菜单项的图标 */
}GuiMenuItemType, *PtrGuiMenuItemType;

typedef		struct	tagGuiMenu
{
	U8					itemNum;			/*	菜单项数 */
	U8					colNum;				/*  菜单列数 */
	U8					*colPos;			/*	菜单各列的相对位置 */
	U8					style;				/*  菜单风格 */
	PtrGuiMenuItemType	*menuItems ;		/*  菜单项 */
}GuiMenuType,*PtrGuiMenu;

typedef		struct		tagMenuEnv
{
	U8		x0;					/* 指定的屏幕显示区域坐标 */
	U8		x1;					/* 由用户填写 */
	U8		y0;
	U8		y1;

	U8		startDispItem;		/* 本屏显示的起始项 */
	U8		maxDispItem;		/* 指定的区域内最大的显示项数 */
	U8		dispItemNum;		/* 本屏幕实际显示的项数 */
}GuiMenuEnvType,*PtrGuiMenuEnv;

typedef		struct	tagRect
{
	U8		left;					/* 左 */
	U8		top;					/* 上 */
	U8		height;					/* 高 */ 
	U8		width;					/* 宽 */
}RectType,*PtrRect;

typedef	struct	tagRandEnv
{
	U32 next 	;
 	U16 randMax	;
}	RandEnvType, *PtrRandEnv;

typedef	struct	tagQryBoxBmp
{
	U8		left;					/* 左 */
	U8		top;					/* 上 */
	U8		height;					/* 高 */ 
	U8		width;					/* 宽 */
	U8		*bmpData;				/* 位图数据 */
} QryBoxBmpType , *PtrQryBoxBmp ;

/*****************************************************************************************/
/******************************************************************************************
*						Gui  部分常量定义
*******************************************************************************************/
/*-----------------------------------------------------------------------------------------
*							单选框
*-----------------------------------------------------------------------------------------*/
#define		SSB_UP_MARGIN					0x02	/* 单选框的上边线和文字的距离 */
#define		SSB_LEFT_MARGIN					0x04
#define		SSB_RIGHT_MARGIN				0x04
#define		SSB_BOTTOM_MARGIN				0x02
#define		SSB_SHADOW_WIDTH				0x02	/* 单选框阴影的宽度 */
/*-----------------------------------------------------------------------------------------
*							菜单
*-----------------------------------------------------------------------------------------*/

#define		SYS_ASC_WIDTH		8
#define		MENU_FONT_HEIGHT	16
#define		MENU_FONT_WIDTH		16
#define		MENU_ICON_WIDTH		16

/*-----------------------------------------------------------------------------------------
*							错误返回值
*-----------------------------------------------------------------------------------------*/
#define		GUI_ERR_UER_ABORT				0xff
#define		GUI_ERR_MEM_OVR					0xfe


/******************************************************************************************
*						系统Bios函数
*******************************************************************************************/
FAR void DirectReadData(U32 StarAddr,U8* BuffPoint,U8 count);
FAR void DirectWriteData(U32 StarAddr,U8* BuffPoint,U8 count);

FAR void SysChinese(U8 x,U8 y,U16 Hz);
FAR void SysAscii(U8 x,U8 y,U8 asc);
FAR void SysPrintString(U8 x,U8 y,U8* str);
FAR void SysLine(U8 x1,U8 y1,U8 x2,U8 y2);
FAR void SysRect(U8 x1,U8 y1,U8 x2,U8 y2);
FAR void SysFillRect(U8 x1,U8 y1,U8 x2,U8 y2);
FAR U8 SysGetSecond();
FAR U8 SysGetMinute();
FAR U8 SysGetHour();
FAR void SysPutPixel(U8 x,U8 y,U8 data);
FAR void SysIconLeftArrow(U8 data);
FAR void SysIconRightArrow(U8 data);
FAR void SysIconUpArrow(U8 data);
FAR void SysIconDownArrow(U8 data);
FAR void SysIconBattery(U8 data);

FAR void SysIconScrollBar(U8 StartPosition,U8 Number,U8 state);
FAR void SysIconScrollBarDownArrow(U8 data);
FAR void SysIconScrollBarUpArrow(U8 data);
FAR void SysIconNumber1(U8 data);
FAR void SysIconNumber2(U8 data);
FAR void SysIconNumber3(U8 data);
FAR void SysIconNumber4(U8 data);
FAR void SysIconDot1(U8 data);
FAR void SysIconDot2(U8 data);
FAR void SysIconDot3(U8 data);
FAR void SysIconF1(U8 data);
FAR void SysIconF2(U8 data);
FAR void SysIconF3(U8 data);
FAR void SysIconF4(U8 data);
FAR void SysIconF5(U8 data);
FAR void SysIconF6(U8 data);
FAR void SysIconF7(U8 data);
FAR void SysIconF8(U8 data);
FAR void SysIconShift(U8 data);
FAR void SysIconCaps(U8 data);
FAR void SysIconRing(U8 data);
FAR void SysIconRingClock(U8 data);
FAR void SysIconSpeaker(U8 data);
FAR void SysIconBell(U8 data);
FAR void SysIconKey(U8 data);

FAR void SysLcdPartClear(U8 x1,U8 y1,U8 x2,U8 y2);
FAR void SysLcdReverse(U8 x1,U8 y1,U8 x2,U8 y2);
FAR	U8 OpenCursorInit(U8 x, U8 y);
FAR void CursorInit(U8 width, U8 high);
FAR U8 OpenCursor();
FAR U8 CloseCursor();
FAR void MoveCursor(U8 x, U8 y);
FAR void SysSaveScreen(U8 x1,U8 y1,U8 x2,U8 y2,U8* BuffPoint);
FAR void SysRestoreScreen(U8 x1,U8 y1,U8 x2,U8 y2,U8* BuffPoint);
FAR void SysPicture(U8 x1,U8 y1,U8 x2,U8 y2,U8* BuffPoint,U8 flag);
void DataBankSwitch(U8 logicStartBank,U8 bankNumber,U16 physicalStartBank);
void GetDataBankNumber(U8 logicStartBank,U16* physicalBankNumber);
FAR void SysCircle(U8 x0,U8 y0,U8 r);
FAR void SysFillCircle(U8 x0,U8 y0,U8 r);
FAR void SysCalcScrBufSize(U8 x1,U8 y1,U8 x2,U8 y2,U16* byteNum);
FAR void SysTimer1Open(U8 times);
FAR void SysTimer1Close();
void MelodyStart(U8 Num,U8* buff,U8 flag);
/******************************************************************************************
*						内存分配函数
*******************************************************************************************/
FAR	void	SysMemInit(U16 start,U16 len) ;
FAR	char*	SysMemAllocate(U16 len);
FAR	U8		SysMemFree(char *p);

FAR	U16		SysRand(PtrRandEnv pRandEnv);
FAR	void	SysSrand(PtrRandEnv pRandEnv ,  U16 seed , U16  randMax);

/******************************************************************************************
*						Gui函数
*******************************************************************************************/
FAR	void	GuiMsgQueInit(void);
FAR	U8      GuiSendMsg(PtrMsg	pMsg);
FAR	U8      GuiPushMsg(PtrMsg pMsg);
FAR	U8		GuiGetMsg(PtrMsg pMsg);
FAR	U8		GuiTranslateMsg(PtrMsg	pMsg);		/* 转换扫描码为字符，或从输入法得到汉字等 */
FAR	U8		GuiInit(void);

FAR	U8		GuiMenu(U8	sel,PtrGuiMenu pMenu ,PtrGuiMenuEnv	pMenuEnv );
FAR	U8		GuiSingleSelBox(U8	origin,PtrGuiMenu pMenu ,PtrGuiMenuEnv	pSglSelBoxEnv );
FAR	U8		GuiQueryBox(U8	sel , U8	infoType , U8	*infoData );

FAR	U8		GuiMsgBox(U8*  msgStr);

/******************************************************************************************
*						编译参数
*******************************************************************************************/

#ifdef	COMPILER_2500AD
	.asm
	.CHIP	6502
	.LINKLIST
	.SYMBOLS
	.DEBUG	ASM
	.LIST	ON
	.optimize	off
	.endasm
#endif	

#endif
