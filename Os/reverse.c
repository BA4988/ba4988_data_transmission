
#include "inc\dictsys.h"
#include "inc\keytable.h"
#include "inc\stdlib.h"

extern U8 BK_SEL;
extern U8 BK_ADRL;
extern U8 BK_ADRH;

extern U8 hello[];
extern U8 head[];
extern U8 str_SysReadCom[];
extern U8 str_DataBankSwitch[];
extern U8 str_SysWriteCom[];
extern U8 str_continue[];
/*
*0x03E4 is 0x0E
*0x03E5 is 0x80
*0x202A is 0x02
*0x202F is 0x2F
start 0x1000
DataBankSwitch(9, 4, 0x0001);
start 0x9000
DataBankSwitch(9, 4, 0x0E80);
start 0xD000
DataBankSwitch(9, 3, 0x0E88);
start 0x
DataBankSwitch(9, 4, (0x07<<2)+0x0E80);
*/

#define DATA4  *(U8*)(0x0003)
#define SBUF   *(U8*)(0x0008)
#define URISR  *(U8*)(0x0009)
#define ADDR4L *(U8*)(0x0211)
#define ADDR4M *(U8*)(0x0212)
#define ADDR4H *(U8*)(0x0213)
#define PB *(U8*)(0x021B)

FAR U8 Reverse()
{
	MsgType msg;
    U8 buffer[8];
    GuiInit(); /*Gui OS 初始化，使用前一定要调用 */
    SysMemInit(MEM_HEAP_START,MEM_HEAP_SIZE);	/*初始化堆*/
    SysLCDClear();
    SysPrintString(11, 23, hello);
	.asm
	nop
	.endasm

    SysOpenCom(UART_MODE, BAUD_115200_BPS, DISABLE_PARITY);

    while (1)
    {
		if(!GuiGetMsg(&msg))		continue;
		if(!GuiTranslateMsg(&msg))	continue;

		if (msg.type == WM_COM)
        {
            U8 size = 5;
            SysReadCom(buffer, &size);
            if (size !=5)
            {
                continue;
            }
            
            PB = buffer[4];
            U16 nbank = *(U16*)(buffer+0);
            ADDR4L = 0x00;
            ADDR4M = buffer[2]<<4;
            ADDR4H = (buffer[3]<<4) + (buffer[2]>>4);

            for (U16 bank=0; bank<nbank; bank++)
            {
                /* 发送1 bank，0x1000字节 */
                for (U16 j=0; j<0x1000; j++)
                {
                    SBUF = DATA4;
                    while ((URISR & 0x20) == 0x00)
                    {
                        .asm
                        nop
                        .endasm
                    }
                    URISR = 0xDF;
                }
            }
        }
		else if (msg.type == WM_CHAR_FUN && msg.param == CHAR_EXIT)
        {
            break;
        }
    }

    SysCloseCom();
    return 0;
}