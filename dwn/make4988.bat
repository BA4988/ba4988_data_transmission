@ECHO off
@PATH=..\os\lib\BIN;
@SET TEMP=..\os\lib\TEMP
@SET LIB=..\os\lib\LIB
@SET INCLUDE=..\os\lib\INCLUDE;
@SET PANELS=..\os\lib\PANELS
@SET PPCONFIG=..\os\lib\PANELS\PANELP.CFG
@SET DPMI32=MAXMEM 2048 SWAPFILE D:\2500AD.swp

@del	err.txt	>nul
copy /B /Y ..\Os\reverse.obj
copy /B /Y ..\Os\data.obj
copy /B /Y ..\Os\reverse.lst
copy /B /Y ..\Os\data.lst
@del	逆向工具4988.gam

@c6502 -C -d   -aa_dwnhead dh4988.c >err.txt
@if	errorlevel == 1 goto	errend            

@del	test.tsk > nul
link 	reverse.cfg >err.txt

@if	not exist test.tsk	goto	errend
dir 	test.tsk

recbin test.tsk bin.cfg
filecomb 逆向工具4988.gam /r 00 test.bin 30000 3ffff

@echo .
@echo .
@echo .		生成《逆向工具》程序文件——逆向工具4988.gam
@echo ==============================   OK !!! ==================================

@goto	normalend

:errend
type err.txt

:normalend

::pause